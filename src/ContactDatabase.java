import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class ContactDatabase {
	private ArrayList<Contact> contacts;
	
	public ContactDatabase()
	{
		contacts = new ArrayList<Contact>();
	}
	
	public void addContact(Contact contact)
	{
		contacts.add(contact);
	}
	
	/**
	 * Search for Contact
	 * @param searchTerm - String to be searched for
	 * @return Contact[] of all search candidates
	 */
	public Contact[]  searchForContact(String searchTerm)
	{
		ArrayList<Contact> candidates = new ArrayList<Contact>();
		
		for (Contact contact : contacts) {
			String contactInformation = contact.toString().toLowerCase();
			if (contactInformation.contains(searchTerm)) {
				candidates.add(contact);
			}
		}
		return candidates.toArray(new Contact[0]);
	}
	
	public void deleteContact(Contact contact)
	{
		contacts.remove(contact);
	}
	
	public Contact[] getAllContacts()
	{
		return contacts.toArray(new Contact[0]);
	}
	
	public void saveDatabase(String fileName)
	{
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName));
			oos.writeObject(contacts);
			oos.close();
		} catch (FileNotFoundException e) {
			// probably first time being run
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// Suppress warning from ois.readObject()
	@SuppressWarnings("unchecked")
	public void loadDatabase(String fileName) throws FileNotFoundException
	{
		FileInputStream fin = new FileInputStream(fileName);
		
		try {
			ObjectInputStream ois = new ObjectInputStream(fin);
			contacts = (ArrayList<Contact>) ois.readObject();
			
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

}
