
public interface View {
	public String get();
	
	public void write(String message);
	public void writeError(String message);
}
