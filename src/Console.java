import java.util.Scanner;


public class Console implements View {
	
	private Scanner keyboard;

	public Console()
	{
		this.keyboard = new Scanner(System.in);
	}

	@Override
	public void write(String message)
	{
		System.out.print(message);

	}

	@Override
	public void writeError(String message)
	{
		System.err.print(message);
	}

	@Override
	public String get() {
		return keyboard.nextLine();
	}

}
