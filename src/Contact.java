import java.io.Serializable;


public class Contact implements Serializable{
	// Default Serial Version ID
	private static final long serialVersionUID = 1L;
	
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String emailAddress;
	
	public Contact(String firstName, String lastName, String phoneNumber, String emailAddress)
	{
		this.setFirstName(firstName);
		this.setLastName(lastName);
		this.setPhoneNumber(phoneNumber);
		this.setEmailAddress(emailAddress);		
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}
	
	@Override
	public String toString()
	{
		return "First Name: " + firstName + "\nLast Name: " + lastName + 
				"\nPhone#: " + phoneNumber + "\nEmail: " + emailAddress + "\n";
	}
}
