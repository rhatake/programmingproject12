import java.io.FileNotFoundException;


public class ContactDatabaseController {
	private View console;
	private ContactDatabase contactDatabase;
	
	private String saveFile;
	
	public ContactDatabaseController(View console, ContactDatabase contactDatabase, String saveFile)
	{
		this.console = console;
		this.contactDatabase = contactDatabase;
		this.saveFile = saveFile;
		
		try {
			writeStatus("Loading Database\n");
			this.contactDatabase.loadDatabase(saveFile);
			writeStatus("Database Loaded\n");
		} catch (FileNotFoundException e) {
			writeStatusError("No Database Found\n");
		}
	}
	
	public void loop()
	{
		boolean done = false;
		
		while (!done) {
			displayMenu();			
			int selectedOption = getResponse();
			
			switch (selectedOption) {
				case 1: addContact(); break;
				case 2: searchContact(); break;
				case 3: displayAllContacts(); break;
				case 4: deleteContact(); break;
				case 5:	done = true;
			}
		}
		
		writeStatus("Saving...\n");
		contactDatabase.saveDatabase(saveFile);
		writeStatus("Saved\n");
	}
	
	private void addContact()
	{
		console.write("\nAdding Contact:\n");
		String firstName = getStringInput("Enter First Name: ");
		String lastName = getStringInput("Enter Last Name: ");
		String number = getStringInput("Enter Phone Number: ");
		String email = getStringInput("Enter Email Address: ");

		contactDatabase.addContact(new Contact(firstName, lastName, number, email));
		console.write("\nContact Added\n");
	}
	
	private void searchContact()
	{
		String query = getStringInput("\nSearch: ");
		
		console.write("\nSearch results:\n\n");
		for (Contact contact : contactDatabase.searchForContact(query)) {
			console.write(contact.toString() + "\n");
		}
	}
	
	private void displayAllContacts()
	{
		console.write("\nContacts:\n");
		for (Contact contact : contactDatabase.getAllContacts()) {
			console.write(contact.toString() + "\n");
		}
	}
	
	private void deleteContact()
	{
		String query = getStringInput("Search: ");
		Contact[] candidates = contactDatabase.searchForContact(query);
		
		console.write("\nSearch results:\n\n");
		int index = 0;	
		for (Contact contact : candidates) {
			console.write(index++ + ") " + contact.toString() + "\n");
		}
		
		console.write("Press Enter to cancel\nNumber to Delete: ");
		
		int itemToDelete = getResponse();
		if (itemToDelete != -1) {
			try {
				contactDatabase.deleteContact(candidates[itemToDelete]);
				console.write("Contact Deleted\n");
			} catch (IndexOutOfBoundsException e) {
				console.write("Invalid Selection: " + e.getMessage());
			}
		}
	}

	/**
	 * get response from menu
	 * @return selection - the positive number selected in response to the menu
	 * -1 - nothing entered
	 */
	private int getResponse()
	{
		String response;

		while (true) {
			try {
				response = console.get();
				
				if (response.isEmpty()) {
					return -1;
				}
				
				int selection = Integer.parseInt(response);
				return selection;
				
			} catch (NumberFormatException e) {
				console.writeError("Invalid selection, try again.\n");
			}
		}
	}
	
	private String getStringInput(String message)
	{
		console.write(message);
		return console.get();
	}
	
	private void writeStatus(String message)
	{
		console.write("[*] - " + message);
	}
	
	private void writeStatusError(String message)
	{
		console.writeError("[!] - " + message);
	}
	
	private void displayMenu()
	{
		console.write("\nAddress Book\n" + 
				"1) Add Contact\n" + 
				"2) Search Contact\n" +
				"3) Display All Contacts\n" +
				"4) Delete Contact\n" +
				"5) Exit\n# ");
	}
}
