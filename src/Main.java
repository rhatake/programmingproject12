
public class Main {

	public static void main(String[] args) {
		Console console = new Console();
		ContactDatabase contactDatabase = new ContactDatabase();
		ContactDatabaseController controller = 
				new ContactDatabaseController(console, contactDatabase, "contactdb.dat");
		
		controller.loop();
	}
}
